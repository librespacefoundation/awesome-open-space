# Contribution Guidelines

Awesome Open-Source Space welcomes contributions through GitLab Merge Requests.

Please make sure that your pull request follows the following guidelines:

- Licensing should be open.
    - Software: In an Open Source initiative approved open-source software licensing scheme
    - Hardware: In an license compatible with Open-source Hardware Association's "Open Hardware Definition"
    - Data & Content: Following the Open Knowledge Foundation's Open Defition.
- If you notice a link to a non-open resource don't hesitate to point it out. 
- The addition is related to open-source space exploration, commercial space, DIY space and astronomy projects, space science, cosmology, or related fields.
- You've ensured that your link isn't already on the site, or already suggested in an outstanding or closed pull request.
- Make an individual pull request for each suggestion.
- Additions should be added in alphabetical order in the relevant category.
- Include a description of the resource along with a name and url.
- Check your spelling and grammar.
- Make sure your text editor is set to remove trailing whitespace.
- New categories, or improvements to the existing categories, are encouraged. (After adding your links, you can optionally run `ruby ./bin/reformat.rb` to regenerate the table of contents and ensure correct sorting.)

Thank you for contributing!
